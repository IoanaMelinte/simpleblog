from django.contrib import admin
from .models import Post, Category

admin.site.register(Post)           # ca cls post sa fie accesibila din zona admin
admin.site.register(Category)