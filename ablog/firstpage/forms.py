from django import forms
from .models import Post, Category

choices=Category.objects.all().values_list('name','name')
choice_list =[]

for item in choices:
    choice_list.append(item)


class PostForm(forms.ModelForm):
    class Meta:
        model = Post
        fields =('titlu', 'titlu_tag', 'autor','category', 'body')

        widgets= {
            'titlu': forms.TextInput(attrs={'class':'form-control', 'placeholder':'This is the place for something'}),
            'titlu_tag': forms.TextInput(attrs={'class': 'form-control'}),
             'autor': forms.TextInput(attrs={'class': 'form-control', 'value':'', 'id':'elder','type':'hidden'}),
            #'autor': forms.Select(attrs={'class': 'form-control'}),
            'category': forms.Select(choices=choice_list, attrs={'class': 'form-control'}),
            'body': forms.Textarea(attrs={'class': 'form-control'}),
        }

class EditForm(forms.ModelForm):
    class Meta:
        model = Post
        fields =('titlu', 'titlu_tag', 'body')
        widgets= {
            'titlu': forms.TextInput(attrs={'class':'form-control', 'placeholder':'This is the place for something'}),
            'titlu_tag': forms.TextInput(attrs={'class': 'form-control'}),
            #'autor': forms.Select(attrs={'class': 'form-control'}),
            'body': forms.Textarea(attrs={'class': 'form-control'}),
        }
