from django.db import models
from django.contrib.auth.models import User
from django.urls import reverse
from datetime import datetime, date

class Category(models.Model):
    name=models.CharField(max_length=255)
    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('Home')


class Post (models.Model):
    titlu = models.CharField(max_length=200)
    titlu_tag = models.CharField(max_length=200, default="Minima[list]")
    autor = models.ForeignKey(User, on_delete=models.CASCADE)    # ca sa nu stearga postarile userului cand il stergem
    body = models.TextField()
    data_postarii = models.DateField(auto_now_add=True)
    category = models.CharField(max_length=200, default="Minimalism")

    def __str__(self):
        return self.titlu + ' by ' + str(self.autor)        #pentru titlu si autor

    def get_absolute_url(self):         # fara apare eroarea No URL to redirect to.  Either provide a url or define a
         return reverse('Home')               # get_absolute_url method on the Model.