from django.urls import path
#from . import views
from .views import Home, Articole, WritePost, UpdatePost, DeletePost, AddCategory, CategoryV, CategoryListV

urlpatterns = [
    #path('', views.home, name="home"),
    path('', Home.as_view(), name='Home'),
    path('articol/<int:pk>', Articole.as_view(), name='detaliiarticol'),
    path('write_post/', WritePost.as_view(), name='write_post'),
    path('add_category/', AddCategory.as_view(), name='add_category'),
    path('articol/edit/<int:pk>', UpdatePost.as_view(), name='update_post'),
    path('articol/<int:pk>/remove', DeletePost.as_view(), name='delete_post'),
    path('category/<str:categ>/', CategoryV, name='category'),
    path('category-list/', CategoryListV, name='category-list'),
]
