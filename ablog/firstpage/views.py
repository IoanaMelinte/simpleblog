from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, UpdateView, DeleteView
from .models import Post, Category
from .forms import PostForm, EditForm
from django.urls import reverse_lazy


# def home(request):
# return render(request, 'home.html', {})

class Home(ListView):
    model = Post
    template_name = 'home.html'
    ordering = ['-data_postarii']

    def get_context_data(self, *args, **kwargs):
        resources_menu = Category.objects.all()
        context = super(Home, self).get_context_data(*args, **kwargs)
        context["resources_menu"] = resources_menu
        return context


def CategoryListV(request):
    resources_menu_list = Category.objects.all()
    return render(request, 'category_list.html', {'resources_menu_list': resources_menu_list})

def CategoryV(request, categ):
    category_posts = Post.objects.filter(category=categ.capitalize().replace('-', ' '))
    return render(request, 'categories.html', {'categ': categ.title().replace('-', ' '), 'category_posts': category_posts})


class Articole(DetailView):
    model = Post
    template_name = 'Articol.html'
    def get_context_data(self, *args, **kwargs):
        resources_menu = Category.objects.all()
        context = super(Articole, self).get_context_data(*args, **kwargs)
        context["resources_menu"] = resources_menu
        return context

class WritePost(CreateView):
    model = Post
    form_class = PostForm
    template_name = 'add_posts.html'
    # fields = '__all__'
    def get_context_data(self, *args, **kwargs):
        resources_menu = Category.objects.all()
        context = super(WritePost, self).get_context_data(*args, **kwargs)
        context["resources_menu"] = resources_menu
        return context

class AddCategory(CreateView):
    model = Category
    template_name = 'add_category.html'
    fields = '__all__'

class UpdatePost(UpdateView):
    model = Post
    form_class = EditForm
    template_name = 'update_post.html'
    # fields = ['titlu', 'titlu_tag', 'autor','body']
    def get_context_data(self, *args, **kwargs):
        resources_menu = Category.objects.all()
        context = super(UpdatePost, self).get_context_data(*args, **kwargs)
        context["resources_menu"] = resources_menu
        return context


class DeletePost(DeleteView):
    model = Post
    template_name = 'delete_post.html'
    success_url = reverse_lazy('Home')
